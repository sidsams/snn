"""
Utils

"""
import numpy as np
import theano
import theano.tensor as T
from random import shuffle


#Gradient Descent Optimsations

def momgrad(cost, params, learning_rate, momentum):
    updates = []
    print 1
    for param in params:
        param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
        updates.append((param, param - learning_rate*param_update))
        updates.append((param_update, momentum*param_update + (1. - momentum)*T.grad(cost, param)))
    return updates

def adagrad(cost, params, learning_rate):
    updates = []
    max_scaling=1e5
    eps = 1. / max_scaling
    for param in params:
        pgrad = T.grad(cost, param)       
        param_hist = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
        new_hist = param_hist + pgrad**2
        scale = T.maximum(eps, T.sqrt(new_hist))
        deltax = (- (learning_rate / scale) * pgrad)
        updates.append((param_hist, new_hist))
        updates.append((param, param + deltax))    
    return updates
    

# General Utils
    
def genTargetFunction(bases=10,tcPeak=10,stimUpper=15,stimLower=-15):
    weights = np.random.rand(bases,1);
    centres = np.array([np.linspace(stimLower,stimUpper,bases)]).T
    width = np.float(stimUpper - stimLower)/bases;
    targetFun = lambda x: fun2(x,weights,tcPeak,centres,width)
    return targetFun;


def initSigmoid(data,tightness,stack=False):
    xMax = np.max(data);
    xMin = np.min(data);
    yMax = tightness + xMax
    yMin = -tightness + xMin
    slope = 4.0/(yMax-yMin);   
    offset = np.float(yMax+yMin)/2
    if stack:
        return np.column_stack((yMax,yMin,slope,offset)) 
    return yMin,yMax,slope,offset
    
    
def siginit(target,base=200):
    dmax = np.max(target,axis=1);
    dmin = np.min(target,axis=1);    
    #base = 200;
    smax = base + dmax
    smin = -base + dmin
    srange = (smax-smin);
    slope = 4.0/srange
    soff = (smax+smin)/2
    return np.column_stack((smax,smin,slope,soff)) 
    
def rateShuffle(rates,weights):
    indlist = range(len(rates))
    shuffle(indlist)
    rates =  [rates[i] for i in indlist]
    weights = weights[0,indlist].reshape(1,-1)
    return rates,weights



def genOU(mu,sig=16):
    th = 1; #1/tau
    mu = mu*np.ones(20/0.001);
    dt = 1e-3;
    t = np.arange(0,20,dt);   
    x = np.zeros(len(t));
    x[0] = mu[0]
    for i in xrange(len(t)-1):
        x[i+1] = x[i]+th*(mu[i]-x[i])*dt+sig*np.sqrt(dt)*np.random.randn();
    
    
   
    return x

def output(X):
        stimUpper = 15;
        stimLower = -15;  
        TCAmp = 1
        dim = 40 
        TCCentre = np.array([np.linspace(stimLower,stimUpper,dim)]).T
        TCWidth = np.float(stimUpper - stimLower)/dim
        Z = np.tile(X,[40,1])
        out =  TCAmp*np.exp(-0.5*(1/TCWidth)*(Z - TCCentre)**2)
        return out

def fun2(X,weights,peak,TCCentre,TCWidth):
        count = len(weights)
        Z = np.tile(X,[count,1])
        out =  peak*np.exp(-0.5*(1/TCWidth**2)*(Z - TCCentre)**2)
        out = np.dot(weights.reshape(1,-1),out)
        return sum(out)

def square(min,max):
    sq = np.zeros([1,20000])[0]
    off = 1000*np.random.randint(1,5,1)[0]
    pul = 1000*np.random.randint(1,5,1)[0]
    ind = 0;
    while ind + pul < len(sq):
        sq[ind:ind+pul] = np.ones(pul)
        ind += off+pul
        off = 1000*np.random.randint(1,5,1)[0]
        pul = 1000*np.random.randint(1,5,1)[0]
    return (max-min)*sq + min    
  
  
def rebin(a, shape):
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).mean(-1).mean(1)

    
# Spiking/Density Estimation    
    
def genSpikes(rates,timeLimit=20):
    spikeList = [];
    
    rateTimeStep = 0.001
    #timeLimit = 20
    for i,row in enumerate(rates):
      maxRate = timeLimit*np.amax(row);   
      maxSpikes = np.cumsum(np.random.exponential(1.0/maxRate,maxRate*timeLimit))
      maxSpikes = maxSpikes[maxSpikes < timeLimit]
      rands = maxRate*np.random.rand(len(maxSpikes))
      allSpikesIndex = (maxSpikes/rateTimeStep).astype(int)
      spikes = [x for ind, x in enumerate(maxSpikes) if row[allSpikesIndex[ind]] > rands[ind]]
      spikeList.append(np.array(spikes))
    return spikeList

def estimateRatesAll(spiketrains,sampletimes,tau):
   outs2 = [];

   for i,row in enumerate(spiketrains):
        outs2.append(estimateRates(row,sampletimes,tau));
   outs2 = np.matrix(outs2)  
   outs = outs2;
   return outs

def FestimateRatesAll(spiketrains,sampletimes,tau):
   outs2 = [];

   for i,row in enumerate(spiketrains):
        outs2.append(FestimateRates(row,sampletimes,tau));
   outs2 = np.matrix(outs2)  
   outs = outs2;
   return outs

#@profile
def FestimateRates(spiketrain,sampletimes,tau):
    output = np.array(sampletimes,dtype=np.float32);
    spiketrain = np.array(spiketrain, dtype=np.float32)
    tau = np.float64(tau)
    for frame, ff in enumerate(output):
      history = spiketrain[(spiketrain < ff)]
      history -=ff
      history /= tau
      
      np.exp(history, out=history)
      output[frame] = sum(history.tolist())/tau
      
      
      
    return output 
    
#@profile
def estimateRates(spiketrain,sampletimes,tau):
    output = np.array(sampletimes,dtype=np.float32);
    spiketrain = np.array(spiketrain, dtype=np.float32)
    tau = np.float64(tau)
    for frame in xrange(len(output)):
      spikeTrain = (spiketrain - output[frame])/tau
      history = spikeTrain[np.where((spikeTrain > -5) & (spikeTrain <= 0))]
      np.exp(history, out=history)
      output[frame] = history.sum()/tau
    return output 
    
