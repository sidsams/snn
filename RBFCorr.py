# -*- coding: utf-8 -*-
"""
Created on Sun May 22 12:55:57 2016
corrgrid
@author: SS
"""

import numpy as np

import matplotlib.pyplot as plt
@np.vectorize
def Norm(x,sigma):
    normVal = 1.0/np.sqrt(2*np.pi*sigma)
    expVal = np.exp(-(x**2)/(2*sigma))
    return normVal*expVal
    
s2 = 3
sOU = 128.0

@np.vectorize
def Corr(i,j):
    cov = Norm(i-j,2*s2)*Norm((i+j)/2, s2/2 + sOU) - Norm(i,s2+sOU)*Norm(j,s2+sOU)
    var = np.sqrt((Norm(0,2*s2)*Norm(i,s2/2 + sOU) - Norm(i,s2 + sOU)**2)*(Norm(0,2*s2)*Norm(i,s2/2 + sOU) - Norm(j,s2 + sOU)**2))
    return cov/var
    
    
inpspace = np.linspace(-15,15,100)
X,Y = np.meshgrid(np.linspace(-15,15,100), np.linspace(15,-15,100))
X = X.T
Y = Y.T

Z = Corr(X, Y)
plt.pcolor(np.linspace(-15,15,100), np.linspace(15,-15,100),Z)

    
#plt.plot(np.linspace(-15,15,101),Corr(0,np.linspace(-15,15,101)))
plt.xlabel('Centre Displacement')
plt.ylabel('Correlation Coefficient')