"""
Spiking Radial Basis Function Network

-Single layer hidden network, single output
 Linear, Somatic and Dendritic integration implementations
 Tuneable parameters:
     'bs' - Batch Size
     'learningRate' - learning Rate
     'kernel' - Kernel width for density estimation
     'firingRate' - Peak value of RBF
     'clus' - Number of clusters
 
-Final MSE values in scoreLin, scoreSom, scoreDen
"""
import numpy as np
import theano
import theano.tensor as T

class Layer(object):
    def __init__(self, initW, initB, activation,opts=[1,1,0],sigPar=None):
        
        # Init params
        self.params = []
        
        #Opts vector defines which parameters are trainable
        #corresponding to [W,b,activation]        
        
        # Weights are trainable
        if opts[0] == 1:
            self.W = theano.shared(value=initW.astype('float64'),name='W',borrow=True)
            self.params.append(self.W)
        else:
            #Weights are fixed
            self.W = initW
        
        # Bias is trainable
        if  opts[1] == 1:
            self.b = theano.shared(value=initB.reshape(-1, 1).astype(theano.config.floatX),
                                   name='b',
                                   borrow=True,
                                   broadcastable=(False, True))
            self.params.append(self.b)
        else:
            #bias is fixed
            self.b = initB
        
        # Activation function is trainable 
        if opts[2] == 1 and activation != None:
            if sigPar is None: #If uninitialised, start with random values
                self.sigtrix = theano.shared(np.random.rand(initW.shape[0], 4))
            else:
                self.sigtrix = theano.shared(sigPar)    
            self.params.append(self.sigtrix)
        else:
            #activation function is fixed
            self.sigtrix = np.random.rand(initW.shape[0], 4)
             
        
        self.activation = activation
        self.opts = opts
       
           
    def output(self, x):
        #Output switch
        lin_output = T.dot(self.W, x) + self.b
        if self.activation == None:
            return lin_output
        if self.activation == 'Sig':
            return T.nnet.sigmoid(lin_output)
        if self.activation == 'Tanh':
            return T.tanh(lin_output)
        if self.activation == 'Soft':
            return T.nnet.softmax(lin_output)
        if self.activation == "Relu":
            return T.nnet.relu(lin_output)
            
        #Default to dendritic non-linearity            
        srange = self.sigtrix[:,0] - self.sigtrix[:,1]
        return T.reshape(srange,[-1,1])*T.nnet.sigmoid(
        T.reshape(self.sigtrix[:,2],[-1,1])*(lin_output - T.reshape(self.sigtrix[:,3],[-1,1])))
        + T.reshape(self.sigtrix[:,1],[-1,1])
        #return (self.sigtrix[:,0] - self.sigtrix[:,1]) * T.nnet.sigmoid(self.sigtrix[:,2]*(lin_output-T.reshape(self.sigtrix[:,3],[-1,1]))) + self.sigtrix[:,1]
        #return (lin_output if self.activation is None else self.activation(lin_output))

class RBFLayer(object):
    # Radial basis function output
    # Input range = stimUpper- stimLower
    # Number of bases = dim
    # TCAmpe = basis function height
    def __init__(self):
        self.stimUpper = 15
        self.stimLower = -15  
        self.TCAmp = 1
        self.dim = 75    
        self.TCWMult = 1
    def output(self,X):
        TCCentre = T.as_tensor_variable(np.array([np.linspace(self.stimLower,self.stimUpper,self.dim)]).T)
        TCWidth = T.as_tensor_variable(self.TCWMult*np.float(self.stimUpper - self.stimLower)/self.dim)
        Z = T.tile(X,[self.dim,1])
        self.out =  self.TCAmp*T.exp(-0.5*(1/TCWidth)*(Z - TCCentre)**2)
        return self.out


class Net(object):
    
    # Generate network
    # Iterate throught parameters and create layers
    def __init__(self, W_init,b_init,activations,opts=[1,1,0]):
        self.layers = []
        for W, b, activation in zip(W_init, b_init, activations):
            self.layers.append(Layer(W,b, activation,opts))

        self.params = []
        for layer in self.layers:
            self.params += layer.params
        
        self.RBF = RBFLayer()
    
    # Regroup network trainable params
    # To be used after network structure is changed
    def resetParams(self):
        self.params = []        
        for layer in self.layers:
            self.params += layer.params
    
    # Output Methods
    def RBFoutput(self,x):
        return self.RBF.output(x)
        
    def output(self, x):       
        x = self.RBF.output(x) 
        for layer in self.layers:
            x = layer.output(x)
        return x
       
    def spikeOutput(self,ybar):
        x = ybar
        for layer in self.layers:
            x = layer.output(x)
        return x
        
    # Cost functions    
    def squaredError(self, x, y):
        return T.mean((self.output(x) - y)**2)
        
    def layerError(self,i, x, y):
        return T.mean((self.layers[i].output(x) - y)**2)
        
    def spikesquaredError(self, x, y):
        return T.mean((self.spikeOutput(x) - y)**2)
        
        
    def cross(self,x,y):
        return T.maximum(0,1-(2*y-1)*self.spikeOutput(x)[0])