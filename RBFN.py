"""
Spiking Radial Basis Function Network

-Single layer hidden network, single output
 Linear, Somatic and Dendritic integration implementations
 Tuneable parameters:
     'bs' - Batch Size
     'learningRate' - learning Rate
     'kernel' - Kernel width for density estimation
     'firingRate' - Peak value of RBF
     'clus' - Number of clusters
 
-Final MSE values in scoreLin, scoreSom, scoreDen
"""

import numpy as np
#import matplotlib.pyplot as plt
import theano
import theano.tensor as T
import utils
import SNN



for iteration in xrange(1): 
    print iteration

    ## SETUP
    fun = utils.genTargetFunction(bases=10, tcPeak=10, stimUpper=15, stimLower=-15) #Target function to approximate

    # Build Network
    neuronCount = 100
    firingRate = 10
    kernel = 3

    initW = []
    initB = [];
    activations = []
    initW.append(np.random.rand(1, neuronCount).astype('float64'))
    initB.append(0)
    activations.append(None)

    X = T.matrix()
    Y = T.vector()
    Z = T.matrix()

    momentum = 0
    rbfn = SNN.Net(initW, initB, activations,opts=[1,0,0])
    rbfn.RBF.dim = neuronCount
    rbfn.RBF.TCWMult = 1



    ## RATE-BASED NETWORK

    # Train linear rate-based network
    cost = rbfn.squaredError(X, Y)
    learning_rate = 0.008
    train = theano.function([X, Y], cost, updates=utils.momgrad(cost, 
                            rbfn.params, learning_rate, momentum), allow_input_downcast=True)
    errorLin = []

    # Batch SGD
    for i in range(6000):
        randX = 30*np.random.rand(1, 5)-15
        err = train(randX, fun(randX))
        errorLin.append(err)

    rateWeights = rbfn.params[0].get_value() #Retrive trained weights
    #plt.figure()
    #plt.plot(np.array(errorLin))  #Error Graph

    # Predict with rate-based network
    rbfn.layers[0].__init__(rateWeights/firingRate, 0, None, [0, 0, 1])
    rbfn.resetParams()
    rbfn.RBF.TCAmp = firingRate
    predict = theano.function([X], rbfn.output(X))

    # Generalisation
    testX = 30*np.random.rand(1, 500)-15
    testY = predict(testX)



    ## SPIKE-BASED NETWORK

    # Generate Input
    stimulus = utils.genOU(0) #Generate Ornstein-Uhlenbeck process
    timeSamples = 20*1000*np.random.rand(1500)
    timeSamples = timeSamples[np.abs(stimulus[(timeSamples).astype(int)]) < 15] #Sample stimulus within RBF range
    timePoints = timeSamples/1000
    timeSamples = timeSamples.astype(int)

    # RBF Outputs
    X = T.matrix()
    RBFInput = stimulus.reshape(1, np.size(stimulus))
    rbfout = theano.function([X], rbfn.RBFoutput(X))
    RBFRates = rbfout(RBFInput)  #Outputs from each basis function


    # Generate spikes/Density estimation
    spikes = utils.genSpikes(RBFRates)
    estRates = utils.FestimateRatesAll(spikes, timePoints, kernel) #Estimate instantaneous firing rates

    ## LINEAR MODEL

    spikePredict = theano.function([Z], rbfn.spikeOutput(Z))
    spikeOutput = spikePredict(estRates)
    cost = rbfn.spikesquaredError(Z, X)
    costFunc = theano.function([Z, X], cost)
    scoreLin = costFunc(estRates, np.array([fun(stimulus[timeSamples])]))  #linear model MSE



    ## SOMATIC MODEL

    #Setup non-linearities
    sMin, sMax, slope, offset = utils.initSigmoid(fun(np.linspace(-15, 15, 100)), 60)
    denData = (sMax-sMin)/(1 + np.exp(-slope*(spikeOutput-offset))) + sMin
    #plt.scatter(fullStimulus[timeSamples],denData,c='r')

    #Implement dendritic integration
    rbfn.layers[0].__init__(rateWeights/firingRate, 0, 1, [0, 0, 1], np.array([[sMax, sMin, slope, offset]]))
    #rbfn.layers.append(Layer(np.array([1]),1,[0,1],np.array([[smax,smin,slope,soff]])))
    rbfn.resetParams()
    spikepredict = theano.function([Z], rbfn.spikeOutput(Z))

    # Train Sigmoid Paramaters
    cost = rbfn.spikesquaredError(Z, X)
    learning_rate = 0.001
    trainSom = theano.function([Z, X], cost, updates=utils.adagrad(cost, rbfn.params, learning_rate))
    costFunc = theano.function([Z, X], cost)
    errorSom = []
    bs = 2 #Batch Size
    for i in xrange(len(timeSamples)/bs): #Batch SGD
        trainSom(estRates[:, i*bs:(i+1)*bs], np.array([fun(stimulus[timeSamples[i*bs:(i+1)*bs]])]))
        errorSom.append(costFunc(estRates, np.array([fun(stimulus[timeSamples])])))
    scoreSom = errorSom[-1]



    ## DENDRITIC MODEL

    clus = 10 #Number of clusters
    newWeights = end = np.zeros([clus, neuronCount]) #Subunit weights
    rateWeights /= firingRate
    for i in xrange(clus): #Restructure layer to separate inputs into clusters
        newWeights[i, i*(neuronCount/clus):(i+1)*(neuronCount/clus)] = rateWeights[0, i*(neuronCount/clus):(i+1)*(neuronCount/clus)]

    sigParams = np.tile(np.array([[sMax, sMin, slope, offset]]), [clus, 1])
    rbfn.layers[0].__init__(newWeights, 0, 1, [0, 0, 1], sigParams) #Introduce non-linearities to original layer
    rbfn.layers.append(SNN.Layer(np.ones([1, clus]), 0, None, [0, 0, 0], np.array([[sMax, sMin, slope, offset]]))) #Add layer regrouping clusters
    rbfn.resetParams()
    learning_rate = 0.0004
    
    # Train dendritic model
    cost = rbfn.spikesquaredError(Z, X)
    trainDen = theano.function([Z, X], cost, updates=utils.adagrad(cost, rbfn.params, learning_rate))
    costFn = theano.function([Z, X], cost)
    errDen = []
    bs = 5 #batch size
    for i in xrange(len(timeSamples)/bs):
        
        aa = trainDen(estRates[:, i*bs:(i+1)*bs], np.array([fun(stimulus[timeSamples[i*bs:(i+1)*bs]])]))
        errDen.append(costFn(estRates, np.array([fun(stimulus[timeSamples])])))

    scoreDen = errDen[-1]
    
    #plt.plot(np.array(nok))
    #spikepredict = theano.function([Z], rbfn.spikeOutput(Z))
    #test1 = spikepredict(rbar)
    #plt.figure()
    #plt.scatter(fullStimulus[timeSamples],test1,c='b')
    print scoreLin, scoreSom, scoreDen
