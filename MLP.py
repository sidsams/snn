"""
Spiking Multilayer Perceptron

-Layer Sizes set in 'layerSizes', first layer is determined from input dim

-Regions for set in 'spikingSections', where index i controls spiking 
 between layer i and i + 1

-Somatic integration set in 'somaticIntegration', where index i denotes 
 layer i+1
 
 
"""
#import pandas as pd
import numpy as np
#import matplotlib.pyplot as plt
import theano
import theano.tensor as T
import sklearn.datasets
from sklearn import preprocessing
import utils
import SNN

#np.random.seed(2)

def nestFuncs(lim):
    """Created nested function calling output of network above a layer (lim)"""
    if lim == len(mlp.layers): #if layer is last layer, return output
        def output(x):
            return x
        return output

    def output(x):
        for li in xrange(len(mlp.layers) - (lim)): #cycle to top of network cascading output fncs
            x = mlp.layers[li+lim].output(x)
        return x
    return output

## LOAD DATA

# Import Data
#airData = pd.read_csv('ghg.gid.site0001.dat',' ',header=None)
#airData2 = pd.read_csv('ghg.gid.site0002.dat',' ',header=None)
#airData3 = pd.read_csv('ghg.gid.site0003.dat',' ',header=None)
#airAll = pd.concat([air,air2,air3],axis=1,ignore_index=True)
#dt = {}
#dt.data = airX[1:15].T
#dt.target = -airX[15:18].T


dt = sklearn.datasets.load_boston()

# Preprocessing
data_scaler = preprocessing.MinMaxScaler() #Values for firing rates must be greater than 0
target_scaler = preprocessing.StandardScaler()
data = data_scaler.fit_transform(dt.data)
targets = target_scaler.fit_transform(dt.target) + 2


multiplier = 1
XA = multiplier*np.transpose(data)
YA = targets.reshape(1, -1)

#window_size = 80
#XA = fil.convolve1d(XA,np.ones(int(window_size))/float(window_size),axis=1)

## NETWORK SETUP
layerSizes = [XA.shape[0], 50, 50, 50, 1]
initW = []
initB = []
activations = []

for n_input, n_output in zip(layerSizes[:-1], layerSizes[1:]):
    initW.append(0.3*np.random.randn(n_output, n_input)) #Initial random weights
    initB.append(np.ones(n_output)) #Set bias to 1
    activations.append('Relu')
activations[-1] = None #Linear activation function for last layer


mlp = SNN.Net(initW, initB, activations)
#mlp.multiplier = multiplier #RBF width parameter



## TRAIN RATE-BASED NETWORK

mlpInput = T.matrix()
mlpTarget = T.matrix()
learningRate = 0.1
cost = mlp.spikesquaredError(mlpInput, mlpTarget)
costFunc = theano.function([mlpInput, mlpTarget], cost)
train = theano.function([mlpInput, mlpTarget], cost,
                        updates=utils.adagrad(cost, mlp.params, learningRate))

# Training with full dataset - overtraining is irrelevant as we are
# measuring MSE change due to spiking

print "Training rate-based network"
trainErr = []
for i in xrange(5000): #epochs
    X = XA
    Y = YA
    current_cost = train(X, Y)
    currErr = current_cost
    trainErr.append(currErr)
    #print currErr

mlpOutput = theano.function([mlpInput], mlp.spikeOutput(mlpInput))
current_output = mlpOutput(XA)
#plt.scatter(range(len(YA.T)),np.sort(YA))
#plt.scatter(range(len(YA.T)),current_output[0,YA.argsort()],c='g')

# Retrive and store trained weights/biases
rateTrained = []
for i, item in enumerate(mlp.layers):
    rateTrained.append(mlp.layers[i].params[0].get_value())
    rateTrained.append(mlp.layers[i].params[1].get_value())

baserate = 1000.0 #Firing rate multiplier
kern = 0.013 #Density estimation kernel



## SPIKING SIMULATION

# Init
X = T.matrix()
Y = T.matrix()

#simulation time - each data point is presented at milisecond timestep
timer = len(dt.data)*0.001

# Train layers in greedy-wise fashion so we iterate through from input layer to output, training and spiking where necessary
spikingSections = [0, 0, 1, 0]
somaticIntegration = [0, 0, 1, 0]
forNext = XA #Input for next section
for section in xrange(3):
    section = section + 1
    print "Starting section: ", section
    rate = baserate#/np.mean(forNext) #Normalise data such that average value matches desired firing rate

    activationFn = activations[section]
    returnLayer = SNN.Layer(np.eye(layerSizes[section])/rate, 
                            np.zeros([layerSizes[section], 1]), activationFn,[0, 0, 0], '').output #Build layer to collect clusters

    index = (section-1)*2
    layerW = rateTrained[index]
    layerB = rateTrained[index+1]
    if spikingSections[section]:
        rateIn = forNext
        spikes = utils.genSpikes(baserate*rateIn, timeLimit = timer) #Generate spikes for layer
        rbar = utils.FestimateRatesAll(spikes,np.arange(0, timer, 0.001), kern) #Density estimation for instantaneous rates
        inp = np.asarray(rbar)

        if somaticIntegration[section]: #Check if layer is using somatic integration
            target = np.dot(layerW, rate*rateIn) + layerB #Determine estimated output magnitude
            sigParams = utils.siginit(target, 200) #Initialise sigmoid at this magnitude
            subUnits = SNN.Layer(layerW, layerB, 1, [0, 0, 1], sigParams)

            #Train sigmoid parameters
            trainErr = []
            arrlen = len(inp.T)
            learning_rate = 1
            upperNet = nestFuncs(section)
            cost = T.mean((upperNet(returnLayer(subUnits.output(X)))- Y)**2)
            train = theano.function([X,Y], cost, updates=utils.adagrad(cost, [subUnits.params[0]], learning_rate))

            for i in xrange(2600): #Batch SGD
                randset = np.random.choice(arrlen, 155)
                current_cost = train(inp[:, randset], YA[0, randset].reshape(1, -1))
                currErr = current_cost
                trainErr.append(currErr)
                #print currErr,2

            forNextFn = theano.function([X], returnLayer(subUnits.output(X)))
            forNext = forNextFn(inp) #Somatic integration output

            #Calculate erro and plot results
            #plt.figure()
            mlp_output = theano.function([X], upperNet(returnLayer(subUnits.output(X))))
            cost = T.mean((upperNet(returnLayer(subUnits.output(X)))- Y)**2)
            costfn = theano.function([X, Y], cost)
            spikeError = costfn(inp, YA)
            current_output = mlp_output(inp)
            #plt.scatter(np.arange(len(YA.T)),current_output[0,YA.argsort()],c='r')
            #plt.scatter(range(len(YA.T)),np.sort(YA))

        else:#Spiking but no somatic integration - calc error
            cost = T.mean((upperNet(returnLayer(subUnits.output(X)))- Y)**2)
            costfn = theano.function([X, Y], cost)
            networkError = costfn(inp, YA) #Overall net error

    else:
        layerOut = SNN.Layer(layerW, layerB, activationFn,[0, 0, 0], '').output #Normal layer output function
        inp = forNext
        forNextFn = theano.function([X], layerOut(X))
        forNext = forNextFn(inp) #layer output
        upperNet = nestFuncs(section-1)
        cost = T.mean((upperNet(X)- Y)**2)
        costfn = theano.function([X, Y], cost)
        networkErr = costfn(inp, YA) ##Overall net error

print "Network Error (MSE):", networkErr

